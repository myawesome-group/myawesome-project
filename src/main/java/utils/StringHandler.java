package utils;

import java.util.regex.Pattern;

public class StringHandler {

    public boolean isClean(String string) {
        return isNull(string) && isEmpty(string) && isEmptySpace(string) && isSpecial(string) && isNumber(string);
    }

    public boolean isNull(String string) {
        return string != null || logFalseStringHandler("null string", null);
    }

    public boolean isEmpty(String string) {
        return !string.isEmpty() || logFalseStringHandler("empty string", string);
    }

    public boolean isEmptySpace(String string) {
        return checkPattern("\\s", "spaces", string);
    }

    public boolean isSpecial(String string) {
        return checkPattern("[!@#$%&*()_+=|<>?{}\\[\\]~-]", "special characters", string);
    }

    public boolean isNumber(String string) {
        return checkPattern("[0-9]", "numbers", string);
    }

    private boolean checkPattern(String regex, String type, String string) {
        return !Pattern.compile(regex).matcher(string).find() || logFalseStringHandler(type, string);
    }

    private boolean logFalseStringHandler(String type, String string) {
        System.out.printf("Silly Goose! You can't have %s :: %s%n", type, string);
        return false;
    }

}
