package core;

import lombok.Getter;
import lombok.Setter;
import repositories.IDictionary;
import utils.GetWords;
import utils.StringHandler;

import java.util.Arrays;

abstract class AbstractDictionary implements IDictionary {

    @Getter @Setter
    private StringHandler stringHandler;
    @Getter @Setter
    private GetWords getWords;

    public AbstractDictionary() {
        stringHandler = new StringHandler();
        getWords = new GetWords();
    }

    protected boolean isWord(String theWord) {
        return stringHandler.isClean(theWord) && (getWords.giveMeGiveMeGiveMe().parallelStream().map(String::toLowerCase)
                .filter(wchar -> wchar.chars().allMatch(Character::isLetter)).anyMatch(toMatch -> isFound(theWord, toMatch)) || logNotFound(theWord));
    }

    private boolean isFound(String theWord, String toMatch) {
        return Arrays.stream(toMatch.chars().mapToObj(c -> (char) c).toArray(Character[]::new))
                .filter(ch -> theWord.toLowerCase().contains(ch.toString())).count() == toMatch.length() && logFound(toMatch);
    }

    private boolean logFound(String toMatch) {
        System.out.printf("Found [%s]%n", toMatch);
        return true;
    }

    private boolean logNotFound(String theWord) {
        System.out.printf("Awww. your word '%s' yield no match...%n", theWord);
        return false;
    }

}
