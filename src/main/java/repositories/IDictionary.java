package repositories;

public interface IDictionary {

    boolean isEnglishWord(String word);

}
