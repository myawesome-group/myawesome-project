package coreTest;

import Setup.Base;
import core.Dictionary;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DictionaryTest extends Base {

    Dictionary dictionary = new Dictionary();

    @Test
    public void nullTest() {
        assertThat(dictionary.isEnglishWord(null)).isFalse();
    }

    @Test
    public void emptyTest() {
        assertThat(dictionary.isEnglishWord("")).isFalse();
    }

    @Test
    public void uniqueTest() {
        assertThat(dictionary.isEnglishWord("workingworking")).isTrue();
    }

    @Test
    public void lowerCaseTest() {
        assertThat(dictionary.isEnglishWord("work")).isTrue();
    }

    @Test
    public void upperCaseTest() {
        assertThat(dictionary.isEnglishWord("WORKING")).isTrue();
    }

    @Test
    public void camelCaseTest() {
        assertThat(dictionary.isEnglishWord("worKing")).isTrue();
    }

    @Test
    public void pascalCaseTest() {
        assertThat(dictionary.isEnglishWord("WorKing")).isTrue();
    }

    @Test
    public void noMatchTest() {
        assertThat(dictionary.isEnglishWord("goose")).isFalse();
    }

    @Test
    public void alphaNumericTest() {
        assertThat(dictionary.isEnglishWord("work4king")).isFalse();
    }

    @Test
    public void specialCharTest() {
        assertThat(dictionary.isEnglishWord("work@king")).isFalse();
    }

    @Test
    public void noSpaceAllowedTest() {
        assertThat(dictionary.isEnglishWord("work king")).isFalse();
    }
}
