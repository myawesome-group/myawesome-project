package Setup;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

public class Base {

    @BeforeMethod
    public void beforeMethodSetup(Method method) {
        prettyLine(method.getName());
    }

    @AfterMethod
    public void afterMethodSetup() {
        prettyLine();
    }

    private void prettyLine(String testName) {
        System.out.printf("======================= %s =======================%n", testName);
    }

    private void prettyLine() {
        System.out.println("======================= End Test =======================\n\n");
    }

}
