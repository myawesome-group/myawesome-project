# Welcome to KITTEN Project!

Folks, today we will look at example on learning Streams that was introduced in year 1542 back with java 8. If you love CSharp (C#) Linq and Lambda expression, you are going to going crazy over Java Streams. You can easily filter, you can easily grab collection or if you want to change value, just use map!

The goal here was to get away from **loops,**  or defining **recursion and iteration** as much as possible.
I will give you 10 bucks if you find a single defined loop in this solution :P

### Now for solution! 
I could have done it with literally 2-3 classes (1 test, 1-2 core) however, I decided to implement  **composition, inheritance, interface and abstract** example and usage.

#### Assumptions! 
Don't care whether dataSet is HastSet for it's unique values or List type as we have control over the data.


# Solution Setup

|                |Version                        | Comments |
|----------------|-------------------------------| ----------|
|JRE		     |`JavaSE-1.8`                   |**`**THIS IS A MUST**`**|
|Eclipse		 |`2020-12 (4.18.0)`             |Feel free to use others|
|TestNG - Plugin |`"Latest for 2020-12"`         |//|
|Maven           |`3.6.3`						 |//|
|                |**Dependencies**				 ||
|lombok          |`3.6.3`						 |For Getters/Setters|
|assertj         |`3.16.1`						 |For Asserts		 |
|testng          |`6.8`						 	 |Test Engine		 |

## Running the solution

1) Update project to pull maven dependency
2) Execute DictionaryTest.java OR DictionaryTest.xml with TestNG

## Discussion Amongst Thyself

```mermaid
sequenceDiagram
client ->> .isEnglishWord(): Give me a valid word bro
.isEnglishWord()->> AbstractWorker: is this a word @ .isWord()?
AbstractWorker-->AbstractWorker: Checking, hold on .isEnglishWord()...
AbstractWorker-->>StringHandler: Is it even a valid string?
StringHandler-x AbstractWorker: Nah, that's some weird language!
AbstractWorker-x .isEnglishWord(): Dude, that's not even valid!!
.isEnglishWord()-x client: (EPIC FAILURE)-- Doh!!, that's not even valid!!
StringHandler->> AbstractWorker: Yea bro, it's legit!
AbstractWorker-->AbstractWorker: Thanks SringHandler, hold on .isEnglishWord()...
AbstractWorker-->AbstractWorker: Filtering and Grabbing Valid List, ok @ .isEnglishWord()...
AbstractWorker->> .isEnglishWord(): ...I Found it! I Found it!...
.isEnglishWord()->> client: Found it, client (PASSED)

```
